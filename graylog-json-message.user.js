// ==UserScript==
// @name         pretty graylog json message with a double click
// @namespace    http://php.html.js.cn/
// @version      0.2
// @description  pretty print graylog json message
// @author       joy2fun <php@html.js.cn>
// @match        https://log.med-education.com/*
// @grant        none
// ==/UserScript==
(function(s) {
    s = document.createElement("script");
    s.type = "text/javascript";
    s.src = "//cdnjs.cloudflare.com/ajax/libs/zepto/1.2.0/zepto.min.js";
    document.head.appendChild(s);
    var t = setInterval(function($){
        if (typeof Zepto !== "function") return;
        clearInterval(t);
        $ = Zepto;
        $(document).on('dblclick', function (e, el, str, mat) {
            el = $(e.target);
            if (el.is('.message-field .field-value')) {
                str = el.first().html();
                if (mat = str.match(/({".*})/)) {
                    el.html('<pre style="white-space: pre-wrap;">'+JSON.stringify(JSON.parse(mat[1].replace(/\\\\/g, '\\')), null, 2)+'</pre>')
                } else {
                    console.log("no json string matched");
                }
            } else {
                console.log("non-message field clicked");
            }
        })
    }, 500);
})();